type TValueType = string | boolean | null | number | object;
export type TValueTypeNames = "string" | "boolean" | "null" | "number" | "link";
export type TNameMapping = {
  string: string;
  boolean: boolean;
  null: null;
  number: number;
  link: TModule;
};

export type TMessageModule = {
  type: "message";
  index: number;
  interface: {
    flushMessage: (message: string) => void;
  };
};

export type TDisplayModule = {
  type: "display";
  index: number;
  interface: {
    drawOnCanvas: (fn: (ctx: CanvasRenderingContext2D) => void) => void;
  };
};

export type TCellModule = {
  type: "cell";
  index: number;
  interface: {
    read: (idx: number) => Value<any>;
    write: (idx: number, value: Value<any>) => void;
  };
};

export type TSwitchModule = {
  type: "switch";
  index: number;
  interface: {
    getState: () => Value<boolean>;
  };
};

export type TModule =
  | TMessageModule
  | TDisplayModule
  | TCellModule
  | TSwitchModule;

class Value<T extends TValueType = TValueType> {
  constructor(public value: T) {}

  toString(): string {
    if (this.isLink()) {
      return `link of type ${this.value.type}`;
    }

    return String(this.value);
  }

  checkType<T extends TValueTypeNames>(
    type: T
  ): this is Value<TNameMapping[T]> {
    switch (type) {
      case "string":
        return this.isString();
      case "number":
        return this.isNumber();
      case "boolean":
        return this.isBoolean();
      case "null":
        return this.isNull();
      case "link":
        return this.isLink();
      default:
        console.error(`Unknown type: ${type}`);
        return false;
    }
  }

  isString(): this is Value<TNameMapping["string"]> {
    return typeof this.value === "string";
  }

  isNumber(): this is Value<TNameMapping["number"]> {
    return typeof this.value === "number";
  }

  isBoolean(): this is Value<TNameMapping["boolean"]> {
    return typeof this.value === "boolean";
  }

  isNull(): this is Value<TNameMapping["null"]> {
    return this.value === null;
  }

  isLink(): this is Value<TNameMapping["link"]> {
    return typeof this.value === "object";
  }
}

export default Value;
