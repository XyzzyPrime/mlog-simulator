import {
  SimRuntimeError,
  SimWrongNumberOfArgumentsError,
  SimWrongTypeError,
} from "./Errors";
import Machine, { TArguments } from "./Machine";
import operations, { enforceTypes } from "./Operations";

class Instructions {
  constructor(public machine: Machine) {}

  set = (args: TArguments) => {
    if (args.length !== 2) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 2,
        provided: args.length,
      });
    }

    const [arg1, arg2] = args;
    if (arg1.type !== "variable")
      throw new SimWrongTypeError({
        expected: "variable",
        provided: arg1.type,
      });

    this.machine.setVariable(arg1.value, this.machine.getArgumentValue(arg2));
  };

  knownOperations: Record<
    string,
    | [["string", "string"], (a: string, b: string) => string]
    | [["number", "number"], (a: number, b: number) => number]
    | [["number"], (a: number) => number]
  > = {
    add: [["number", "number"], (a: number, b: number) => a + b],
  };

  op = (args: TArguments) => {
    if (args.length < 2) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 2,
        provided: args.length,
      });
    }

    const [operationName, dest, ...rest] = args;
    const values = rest.map((arg) => this.machine.getArgumentValue(arg));
    if (operationName.type !== "variable") {
      throw new SimWrongTypeError({
        provided: operationName.type,
        expected: "variable",
      });
    }

    if (dest.type !== "variable") {
      throw new SimWrongTypeError({
        provided: dest.type,
        expected: "variable",
      });
    }

    const operation =
      operations[operationName.value as keyof typeof operations];

    if (operation === undefined) {
      throw new SimRuntimeError(`Unknown operation '${operationName.value}'`);
    }

    const result = operation(...values);

    this.machine.setVariable(dest.value, result);
  };

  jump = (args: TArguments) => {
    if (args.length < 2) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 2,
        provided: args.length,
      });
    }

    const [dest, operationName, ...rest] = args;
    const values = rest.map((arg) => this.machine.getArgumentValue(arg));
    if (operationName.type !== "variable") {
      throw new SimWrongTypeError({
        provided: operationName.type,
        expected: "variable",
      });
    }

    if (dest.type !== "constant") {
      throw new SimWrongTypeError({
        provided: dest.type,
        expected: "variable",
      });
    }

    if (typeof dest.value !== "number") {
      throw new SimWrongTypeError({
        provided: dest.type,
        expected: "number",
      });
    }

    const operation =
      operations[operationName.value as keyof typeof operations];

    if (operation === undefined) {
      throw new SimRuntimeError(`Unknown operation '${operationName.value}'`);
    }
    const result = operation(...values);
    if (!result.isBoolean()) {
      throw new SimRuntimeError(
        `Operation '${operationName.value}' does not return boolean`
      );
    }

    if (result.value) {
      this.machine.currentCommand = dest.value;
    }
  };

  print = (args: TArguments) => {
    if (args.length !== 1) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 1,
        provided: args.length,
      });
    }

    const [arg] = args;
    const value = this.machine.getArgumentValue(arg);
    const str = this.machine.getStringFromValue(value);
    this.machine.printBuffer.push(str);
  };

  printFlush = (args: TArguments) => {
    if (args.length !== 1) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 1,
        provided: args.length,
      });
    }

    const [arg] = args;
    const value = this.machine.getArgumentValue(arg);

    if (!value.isLink()) {
      throw new SimWrongTypeError({
        expected: "link",
        provided: String(value.value),
      });
    }

    if (value.value.type !== "message") {
      throw new SimWrongTypeError({
        expected: "message link",
        provided: String(value),
      });
    }

    value.value.interface.flushMessage(this.machine.printBuffer.join(""));
    this.machine.printBuffer = [];
  };

  draw = (args: TArguments) => {
    if (args.length < 1) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 1,
        provided: args.length,
      });
    }

    const [operationName, ...args2] = args;

    if (operationName.type !== "variable") {
      throw new SimWrongTypeError({
        provided: operationName.type,
        expected: "variable",
      });
    }

    const n = "number";

    const values = args2.map((arg) => this.machine.getArgumentValue(arg));

    /** Need to capture those variables in local scope */
    const { currentDrawFill, currentDrawStroke, currentDrawAlpha } =
      this.machine;

    const setupContext = (ctx: CanvasRenderingContext2D) => {
      ctx.fillStyle = currentDrawFill;
      ctx.strokeStyle = currentDrawFill;
      ctx.lineWidth = currentDrawStroke;
      ctx.lineCap = "square";
      ctx.globalAlpha = currentDrawAlpha;
    };

    const numbersToColor = (...args: number[]) => {
      args = args.map((arg) => Math.max(0, Math.min(255, arg)));
      const [r, g, b] = args;
      return `rgb(${r}, ${g}, ${b})`;
    };

    const WIDTH = 80;
    const HEIGHT = 80;

    switch (operationName.value) {
      case "clear":
        enforceTypes([n, n, n], (r, g, b) => {
          this.machine.drawBuffer.push((ctx) => {
            setupContext(ctx);
            ctx.fillStyle = numbersToColor(r, g, b);
            ctx.globalAlpha = 1;
            ctx.fillRect(0, 0, WIDTH, HEIGHT);
          });
        })(...values);
        break;
      case "color":
        enforceTypes([n, n, n, n], (r, g, b, a) => {
          this.machine.currentDrawFill = numbersToColor(r, g, b);
          this.machine.currentDrawAlpha = a / 255;
        })(...values);
        break;
      case "stroke":
        enforceTypes([n], (width) => {
          this.machine.currentDrawStroke = width;
        })(...values);
        break;
      case "line":
        enforceTypes([n, n, n, n], (x1, y1, x2, y2) => {
          this.machine.drawBuffer.push((ctx) => {
            setupContext(ctx);
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.stroke();
          });
        })(...values);
        break;
      case "rect":
        enforceTypes([n, n, n, n], (x, y, w, h) => {
          this.machine.drawBuffer.push((ctx) => {
            setupContext(ctx);
            ctx.fillRect(x, y, w, h);
          });
        })(...values);
        break;
      case "fillrect":
        enforceTypes([n, n, n, n], (x, y, w, h) => {
          this.machine.drawBuffer.push((ctx) => {
            setupContext(ctx);
            ctx.strokeRect(x, y, w, h);
          });
        })(...values);
        break;
      default:
        throw new SimRuntimeError(`Unknown operation '${operationName.value}'`);
    }
  };

  drawFlush = (args: TArguments) => {
    if (args.length !== 1) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 1,
        provided: args.length,
      });
    }

    const [arg] = args;
    const value = this.machine.getArgumentValue(arg);

    if (!value.isLink()) {
      throw new SimWrongTypeError({
        expected: "link",
        provided: String(value.value),
      });
    }

    if (value.value.type !== "display") {
      throw new SimWrongTypeError({
        expected: "display link",
        provided: String(value),
      });
    }

    const buffer = this.machine.drawBuffer;
    this.machine.drawBuffer = [];

    value.value.interface.drawOnCanvas((ctx) => buffer.forEach((f) => f(ctx)));
  };

  write = (args: TArguments) => {
    if (args.length !== 3) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 3,
        provided: args.length,
      });
    }

    const [valArg, cellArg, idxArg] = args;
    const val = this.machine.getArgumentValue(valArg);
    const cell = this.machine.getArgumentValue(cellArg);
    const idx = this.machine.getArgumentValue(idxArg);

    if (!cell.isLink()) {
      throw new SimWrongTypeError({
        expected: "link",
        provided: String(cell.value),
      });
    }

    if (cell.value.type !== "cell") {
      throw new SimWrongTypeError({
        expected: "display link",
        provided: String(cell.value),
      });
    }

    if (!idx.isNumber()) {
      throw new SimWrongTypeError({
        expected: "number",
        provided: String(idx.value),
      });
    }

    cell.value.interface.write(idx.value, val);
  };

  read = (args: TArguments) => {
    if (args.length !== 3) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 3,
        provided: args.length,
      });
    }

    const [destArg, cellArg, idxArg] = args;
    const cell = this.machine.getArgumentValue(cellArg);
    const idx = this.machine.getArgumentValue(idxArg);

    if (!cell.isLink()) {
      throw new SimWrongTypeError({
        expected: "link",
        provided: String(cell.value),
      });
    }

    if (cell.value.type !== "cell") {
      throw new SimWrongTypeError({
        expected: "display link",
        provided: String(cell.value),
      });
    }

    if (!idx.isNumber()) {
      throw new SimWrongTypeError({
        expected: "number",
        provided: String(idx.value),
      });
    }

    if (destArg.type !== "variable") {
      throw new SimWrongTypeError({
        provided: destArg.type,
        expected: "variable",
      });
    }

    const newValue = cell.value.interface.read(idx.value);

    this.machine.setVariable(destArg.value, newValue);
  };

  sensor = (args: TArguments) => {
    if (args.length !== 3) {
      throw new SimWrongNumberOfArgumentsError({
        expected: 3,
        provided: args.length,
      });
    }

    const [destArg, linkArg, propArg] = args;
    const link = this.machine.getArgumentValue(linkArg);

    if (!link.isLink()) {
      throw new SimWrongTypeError({
        expected: "link",
        provided: String(link.value),
      });
    }

    if (link.value.type !== "switch") {
      throw new SimWrongTypeError({
        expected: "switch link",
        provided: String(link.value),
      });
    }

    if (propArg.type !== "variable") {
      throw new SimWrongTypeError({
        expected: "variable",
        provided: String(propArg.value),
      });
    }

    if (destArg.type !== "variable") {
      throw new SimWrongTypeError({
        provided: destArg.type,
        expected: "variable",
      });
    }

    if (propArg.value !== "@enabled") {
      throw new SimRuntimeError(
        `Only @enabled on switches are supported, you asked for ${propArg.value}.`
      );
    }

    const newValue = link.value.interface.getState();

    this.machine.setVariable(destArg.value, newValue);
  };
}

export default Instructions;
