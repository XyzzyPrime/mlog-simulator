import { SimRuntimeError } from "./Errors";
import Instructions from "./Instructions";
import Value, { TModule } from "./Value";

export type TConstantNode = {
  type: "constant";
  value: number | string;
};

export type TVariableNode = {
  type: "variable";
  value: string;
};

export type TArguments = (TConstantNode | TVariableNode)[];

export type TError = {
  message: string;
  lineNumber: number;
};

export type TCommandNode = {
  line_number: number;
  type: "command";
  value: string;
  arguments: TArguments;
};

class Machine {
  instructions: Instructions;
  variables: Record<string, Value> = {};
  variablesFromEnvironment: Record<string, Value> = {};
  modulesByType: Record<TModule["type"], TModule[]> = {
    message: [],
    display: [],
    cell: [],
    switch: [],
  };
  /** pointer on currently executed command */
  currentCommand = 0;
  /** Current line for better error messages */
  currentLine = 0;
  printBuffer: string[] = [];
  drawBuffer: ((ctx: CanvasRenderingContext2D) => void)[] = [];
  currentDrawStroke = 1;
  currentDrawFill = "rgb(0, 0, 0)";
  currentDrawAlpha = 1;
  startTime = 0;
  instructionsExecuted = 0;
  shouldStop = false;

  constructor(
    modules: TModule[],
    public commands: TCommandNode[],
    public setError: (e: TError) => void,
    public instructionsPerSecond: number
  ) {
    this.instructions = new Instructions(this);

    modules
      .sort((a, b) => a.index - b.index)
      .forEach((module) => {
        this.modulesByType[module.type].push(module);
      });

    Object.keys(this.modulesByType).forEach((type) =>
      this.modulesByType[type as TModule["type"]].forEach(
        (module, i) =>
          (this.variablesFromEnvironment[
            `${this.variablePrefix}${type}${i + 1}`
          ] = new Value(module))
      )
    );

    (window as any).machine = this;
  }

  sleepUntilNextTick = async () => {
    const nextTickTime =
      this.startTime +
      (this.instructionsExecuted * 1000) / this.instructionsPerSecond;

    return new Promise((resolve) =>
      setTimeout(resolve, Math.max(0, nextTickTime - window.performance.now()))
    );
  };

  /** Runs main loop of this Machine */
  run = async (onStop: () => void) => {
    try {
      this.startTime = window.performance.now();
      this.instructionsExecuted = 0;

      while (!this.shouldStop) {
        await this.sleepUntilNextTick();
        this.tick();
        this.instructionsExecuted++;
      }
    } finally {
      onStop();
    }
  };

  /* Execute current instruction */
  tick = () => {
    if (
      this.currentCommand < 0 ||
      this.currentCommand >= this.commands.length
    ) {
      this.currentCommand = 0;
    }

    const command = this.commands[this.currentCommand];
    console.log(
      `Executing command ${command.value} on line ${command.line_number}`
    );
    this.currentLine = command.line_number;
    this.currentCommand++;
    try {
      this.executeCommand(command);
    } catch (e) {
      console.error(
        `Error happened in Machine on line ${this.currentLine}: '${e}'`
      );
      this.setError({
        message: String(e),
        lineNumber: this.currentLine,
      });
      this.stop();
    }
  };

  stop = () => {
    this.shouldStop = true;
  };

  executeCommand = (command: TCommandNode) => {
    switch (command.value.toLowerCase()) {
      case "set":
        return this.instructions.set(command.arguments);
      case "op":
        return this.instructions.op(command.arguments);
      case "jump":
        return this.instructions.jump(command.arguments);
      case "print":
        return this.instructions.print(command.arguments);
      case "printflush":
        return this.instructions.printFlush(command.arguments);
      case "draw":
        return this.instructions.draw(command.arguments);
      case "drawflush":
        return this.instructions.drawFlush(command.arguments);
      case "write":
        return this.instructions.write(command.arguments);
      case "read":
        return this.instructions.read(command.arguments);
      case "sensor":
        return this.instructions.sensor(command.arguments);
      default:
        throw new SimRuntimeError(`Unknown command: '${command.value}'`);
    }
  };

  /** This prefix is required because user can use variable names like `prototype` */
  variablePrefix = "variable_";
  setVariable = (variableName: string, value: Value) => {
    this.variables[`${this.variablePrefix}${variableName}`] = value;
  };

  getVariable = (variableName: string): Value => {
    switch (variableName) {
      case "@counter":
        return new Value(this.currentCommand);
      case "true":
        return new Value(true);
      case "false":
        return new Value(false);
      default:
        const name = `${this.variablePrefix}${variableName}`;
        return (
          this.variablesFromEnvironment[name] ??
          this.variables[name] ??
          new Value(null)
        );
    }
  };

  /** Evaluates argument - currently only expanding variables */
  getArgumentValue = (argument: TArguments[number]): Value => {
    if (argument.type === "constant") {
      return new Value(argument.value);
    }

    return this.getVariable(argument.value);
  };

  getStringFromValue = (val: Value) => {
    return String(val.value);
  };
}

export default Machine;
