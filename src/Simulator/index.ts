import { SimBuildtimeError } from "./Errors";
import Machine, { TCommandNode, TError } from "./Machine";
import { TMessageModule, TModule } from "./Value";

type TNameToken = {
  type: "name";
  value: string;
};

type TStringToken = {
  type: "string";
  value: string;
};

type TNumberToken = {
  type: "number";
  value: number;
};

type TInvalidToken = {
  type: "invalid";
};

type TNewlineToken = {
  type: "newline";
};

type TTokenValue =
  | TNameToken
  | TStringToken
  | TNumberToken
  | TNewlineToken
  | TInvalidToken;

type TToken = {
  token: TTokenValue;
  line_number: number;
};

class Simulator {
  runCode = ({
    code,
    modules,
    setError,
    instructionsPerSecond,
    onStop,
  }: {
    code: string;
    modules: TModule[];
    setError: (e: TError) => void;
    instructionsPerSecond: number;
    onStop: () => void;
  }) => {
    try {
      const tokens = this.tokenizer(code);
      const commands = this.parser(tokens);
      const machine = new Machine(
        modules,
        commands,
        setError,
        instructionsPerSecond
      );
      machine.run(onStop);
      return machine;
    } catch (e) {
      if (e instanceof SimBuildtimeError) {
        setError({
          message: String(e),
          lineNumber: e.lineNumber,
        });
      } else {
        console.log(String(e));
      }
      onStop();
      return null;
    }
  };

  tokenizer = (code: string) => {
    code += "\n"; // In case last line does not end with newline

    let current_i = 0;
    let current_line = 1;

    const tokens: TToken[] = [];

    const addToken = (token: TTokenValue) => {
      tokens.push({
        token,
        line_number: current_line,
      });
    };

    const throwError = (message: string) => {
      throw new SimBuildtimeError(current_line, message);
    };

    const isNewline = (char: string) => {
      return char === "\n";
    };

    const isWhitespaceChar = (char: string) => /\s/.test(char);
    const isNumberChar = (char: string) => /[0-9]/.test(char);
    const isStringStartChar = (char: string) => /["']/.test(char);
    const isVariableChar = (char: string) => /[a-z@_]/i.test(char);

    /** Returns current character from parsed code */
    const getChar = () => {
      if (current_i >= code.length) {
        throwError("unexpected EOF");
      }
      const char = code[current_i];
      return char;
    };

    /** Returns current character and moves pointer */
    const consumeChar = () => {
      const char = getChar();
      current_i++;
      if (isNewline(char)) {
        current_line++;
      }
      return char;
    };

    const assertNotNewline = () => {
      if (isNewline(getChar())) {
        throwError("Unexpected newline");
      }
    };

    while (current_i < code.length) {
      // Newline
      if (isNewline(getChar())) {
        consumeChar();
        addToken({
          type: "newline",
        });
        continue;
      }

      // Whitespace
      if (isWhitespaceChar(getChar())) {
        consumeChar();
        continue;
      }

      // Number
      if (isNumberChar(getChar())) {
        let value = "";

        while (isNumberChar(getChar())) {
          assertNotNewline();
          value += consumeChar();
        }

        addToken({
          type: "number",
          value: Number(value),
        });
        continue;
      }

      // String
      if (isStringStartChar(getChar())) {
        const startChar = getChar();
        consumeChar();
        let value = "";

        while (getChar() !== startChar) {
          assertNotNewline();
          value += consumeChar();
        }

        consumeChar();
        addToken({
          type: "string",
          value,
        });
        continue;
      }

      // Variable
      if (isVariableChar(getChar())) {
        let value = "";

        while (isVariableChar(getChar()) || isNumberChar(getChar())) {
          assertNotNewline();
          value += consumeChar();
        }

        addToken({
          type: "name",
          value,
        });
        continue;
      }

      // Unexpected character, invalidates current line
      while (!isNewline(getChar())) consumeChar();
      addToken({
        type: "invalid",
      });
    }

    return tokens;
  };

  parser = (tokens: TToken[]) => {
    const commands = [];
    let current_i = 0;
    let current_line = 0;

    const throwError = (message: string) => {
      throw new SimBuildtimeError(current_line, message);
    };

    const getToken = () => {
      if (current_i >= tokens.length) {
        throwError("Unexpected EOF");
      }

      const token = tokens[current_i];
      current_line = token.line_number;
      return token;
    };

    const consumeToken = () => {
      const token = getToken();
      current_i++;
      return token;
    };

    while (current_i < tokens.length) {
      const token = getToken();
      if (token.token.type === "name") {
        let commandNode: TCommandNode | null = {
          line_number: token.line_number,
          type: "command",
          value: token.token.value,
          arguments: [],
        };
        consumeToken();

        while (getToken().token.type !== "newline") {
          const token = getToken();

          if (token.token.type === "invalid") {
            while (getToken().token.type !== "newline") consumeToken();
            commandNode = null;
            continue;
          }

          if (token.token.type === "name") {
            consumeToken();
            commandNode?.arguments.push({
              type: "variable",
              value: token.token.value,
            });
            continue;
          }

          if (token.token.type === "number" || token.token.type === "string") {
            consumeToken();
            commandNode?.arguments.push({
              type: "constant",
              value: token.token.value,
            });
            continue;
          }

          throwError(`Unknown token: '${JSON.stringify(token.token)}'`);
        }

        if (commandNode !== null) {
          commands.push(commandNode);
        }

        continue;
      }

      if (getToken().token.type === "invalid") {
        while (getToken().token.type !== "newline") consumeToken();
        continue;
      }

      if (getToken().token.type === "newline") {
        consumeToken();
        continue;
      }
    }

    return commands;
  };
}

export type { TMessageModule, TModule, TError };

export default Simulator;
