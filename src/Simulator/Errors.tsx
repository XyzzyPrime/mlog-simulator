class SimError extends Error {}

export class SimBuildtimeError extends SimError {
  constructor(public lineNumber: number, ...params: any[]) {
    super(...params);
  }
}

export class SimRuntimeError extends SimError {}

export class SimWrongNumberOfArgumentsError extends SimRuntimeError {
  expected: number;
  provided: number;

  constructor(
    { expected, provided }: { expected: number; provided: number },
    ...params: any[]
  ) {
    super(...params);
    this.expected = expected;
    this.provided = provided;
  }

  get message() {
    return `Wrong number of Arguments. Expected ${this.expected}, but received ${this.provided}`;
  }
}

export class SimWrongTypeError extends SimRuntimeError {
  expected: string;
  provided: string;

  constructor(
    { expected, provided }: { expected: string | string[]; provided: string },
    ...params: any[]
  ) {
    super(...params);
    if (typeof expected === "object") {
      this.expected = expected.join(" or ");
    } else {
      this.expected = expected;
    }
    this.provided = provided;
  }

  get message() {
    return `Wrong Type. Expected type ${this.expected}, but received '${this.provided}'`;
  }
}

export class SimNotImplementedError extends SimRuntimeError {
  constructor(public featureName: string, ...params: any[]) {
    super(...params);
  }

  get message() {
    return `Not Supported. Feature '${this.featureName}' is not yet supported.`;
  }
}
