import {
  SimNotImplementedError,
  SimWrongNumberOfArgumentsError,
  SimWrongTypeError,
} from "./Errors";
import Value, { TValueTypeNames } from "./Value";

export const enforceTypes =
  <T extends (TValueTypeNames | TValueTypeNames[])[]>(
    types: T,
    fnc: (...args: any[]) => any
  ) =>
  (...args: Value[]) => {
    if (types.length !== args.length)
      throw new SimWrongNumberOfArgumentsError({
        expected: types.length,
        provided: args.length,
      });

    types.forEach((type, index) => {
      const allowedTypes = Array.isArray(type) ? type : [type];
      if (!allowedTypes.some((type) => args[index].checkType(type)))
        throw new SimWrongTypeError({
          expected: type,
          provided: String(args[index]),
        });
    });

    return new Value(fnc(...args.map((arg) => arg.value)));
  };

const n = "number";
const any: TValueTypeNames[] = ["string", "number", "null", "link", "boolean"];

// REference code in Mindustry: https://github.com/Anuken/Mindustry/blob/aa7936100ea41e0c1f7af5640050906415903753/core/src/mindustry/logic/LogicOp.java
const operations = {
  add: enforceTypes([n, n], (a, b) => a + b),
  sub: enforceTypes([n, n], (a, b) => a - b),
  mul: enforceTypes([n, n], (a, b) => a * b),
  div: enforceTypes([n, n], (a, b) => a / b),
  idiv: enforceTypes([n, n], (a, b) => Math.floor(a / b)),
  mod: enforceTypes([n, n], (a, b) => a % b),
  pow: enforceTypes([n, n], (a, b) => Math.pow(a, b)),

  // eslint-disable-next-line eqeqeq
  equal: enforceTypes([any, any], (a, b) => a == b),
  // eslint-disable-next-line eqeqeq
  notEqual: enforceTypes([any, any], (a, b) => a != b),
  land: enforceTypes([any, any], (a, b) => Boolean(a && b)),
  lessThan: enforceTypes([n, n], (a, b) => a < b),
  lessThanEq: enforceTypes([n, n], (a, b) => a <= b),
  greaterThan: enforceTypes([n, n], (a, b) => a > b),
  greaterThanEq: enforceTypes([n, n], (a, b) => a >= b),
  strictEqual: enforceTypes([n, n], (a, b) => a === b),

  shl: enforceTypes([n, n], (a, b) => a << b),
  shr: enforceTypes([n, n], (a, b) => a >> b),
  or: enforceTypes([n, n], (a, b) => a | b),
  and: enforceTypes([n, n], (a, b) => a & b),
  xor: enforceTypes([n, n], (a, b) => a ^ b),
  not: enforceTypes([n], (a) => ~a),

  max: enforceTypes([n, n], (a, b) => Math.max(a, b)),
  min: enforceTypes([n, n], (a, b) => Math.min(a, b)),
  angle: enforceTypes([n, n], (a, b) => {
    throw new SimNotImplementedError("operation angle");
  }),
  len: enforceTypes([n, n], (a, b) => Math.sqrt(a * a + b * b)),
  noise: enforceTypes([n, n], (a, b) => {
    throw new SimNotImplementedError("operation simplex noise");
  }),
  abs: enforceTypes([n], (a) => Math.abs(a)),
  log: enforceTypes([n], (a) => Math.log(a)),
  log10: enforceTypes([n], (a) => Math.log10(a)),
  sin: enforceTypes([n], (a) => Math.sin(a * 0.017453292519943295)),
  cos: enforceTypes([n], (a) => Math.cos(a * 0.017453292519943295)),
  tan: enforceTypes([n], (a) => Math.tan(a * 0.017453292519943295)),
  floor: enforceTypes([n], (a) => Math.floor(a)),
  ceil: enforceTypes([n], (a) => Math.ceil(a)),
  sqrt: enforceTypes([n], (a) => Math.sqrt(a)),
  rand: enforceTypes([n], (a) => Math.random() * a),

  always: enforceTypes([], () => true),
};

export default operations;
