import React, { useState } from "react";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core";
import { StylesProvider } from "@material-ui/core/styles";
import { createGlobalStyle, ThemeProvider } from "styled-components";

import Layout from "Components/Layout";
import SimulatorStore, { simulatorStoreContext } from "Stores/SimulatorStore";

const theme = createMuiTheme({});

const GlobalStyle = createGlobalStyle`
  html {
    height: 100%;
  }

  html, body, #root {
    margin: 0;
    padding: 0;
    display: flex;
    flex-grow: 1;
    flex-direction: row;
    box-sizing: border-box;
  }
`;

function App() {
  const [simulatorStore] = useState(() => new SimulatorStore());

  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <MuiThemeProvider theme={theme}>
          <simulatorStoreContext.Provider value={simulatorStore}>
            <GlobalStyle />
            <Layout />
          </simulatorStoreContext.Provider>
        </MuiThemeProvider>
      </ThemeProvider>
    </StylesProvider>
  );
}

export default App;
