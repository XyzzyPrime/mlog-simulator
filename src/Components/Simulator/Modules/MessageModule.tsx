import React, { useState } from "react";
import { Card, CardContent, Typography } from "@material-ui/core";
import styled, { css } from "styled-components";

import { useRegisteredModule } from "Utils/hooks";

type Props = {
  index: number;
};

const MessageModule = ({ index }: Props) => {
  const [messages, setMessages] = useState<string[]>([]);

  useRegisteredModule({
    type: "message",
    index,
    interface: {
      flushMessage: (message) =>
        setMessages((oldMessages) => [message, ...oldMessages].slice(0, 20)),
    },
  });

  return (
    <Container variant="outlined">
      <CardContent>
        <Typography>
          <b>message{index}</b>
        </Typography>
        <MessagesContainer>
          {messages.map((message, i) => (
            <MessageContainer key={i}>
              <Typography>{i === 0 ? <b>{message}</b> : message}</Typography>
            </MessageContainer>
          ))}
        </MessagesContainer>
      </CardContent>
    </Container>
  );
};

const Container = styled(Card)`
  ${({ theme }) => css`
    margin: ${theme.spacing(1)}px;
  `}
`;

const MessagesContainer = styled.div`
  height: 100px;
  overflow-y: auto;

  ${({ theme }) => css`
    margin-top: ${theme.spacing(2)}px;
  `}
`;

const MessageContainer = styled.div``;

export default MessageModule;
