import React, { useRef } from "react";
import { Card, CardContent, Typography } from "@material-ui/core";
import styled, { css } from "styled-components";

import { useRegisteredModule } from "Utils/hooks";

type Props = {
  index: number;
};

const DisplayModule = ({ index }: Props) => {
  const canvasContainer = useRef<HTMLCanvasElement>(null);

  useRegisteredModule({
    type: "display",
    index,
    interface: {
      drawOnCanvas: (fn) => {
        const ctx = canvasContainer.current?.getContext("2d");
        if (ctx) {
          fn(ctx);
        }
      },
    },
  });

  return (
    <Container variant="outlined">
      <CardContent>
        <Typography>
          <b>display{index}</b>
        </Typography>
        <Canvas ref={canvasContainer} width={80} height={80} />
      </CardContent>
    </Container>
  );
};

const Container = styled(Card)`
  ${({ theme }) => css`
    margin: ${theme.spacing(1)}px;
  `}
`;

const Canvas = styled.canvas`
  border: 2px solid black;
  width: 200px;
  height: 200px;

  ${({ theme }) => css`
    margin-top: ${theme.spacing(2)}px;
  `}
`;

export default DisplayModule;
