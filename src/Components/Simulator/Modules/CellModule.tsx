import React, { useRef, useState } from "react";
import { Card, CardContent, Typography } from "@material-ui/core";
import styled, { css } from "styled-components";

import { useRegisteredModule } from "Utils/hooks";
import Value from "Simulator/Value";

type Props = {
  index: number;
};

const CellModule = ({ index }: Props) => {
  const [memory, setMemory] = useState<Record<number, Value<any> | undefined>>(
    {}
  );
  const memoryRef = useRef<typeof memory>(memory);
  memoryRef.current = memory;

  const capacity = 64;

  useRegisteredModule({
    type: "cell",
    index,
    interface: {
      read: (idx) => {
        return memoryRef.current[idx] ?? new Value(null);
      },
      write: (idx, value) =>
        setMemory((oldMemory) => ({
          ...oldMemory,
          [idx]: value,
        })),
    },
  });

  return (
    <Container variant="outlined">
      <CardContent>
        <Typography>
          <b>cell{index}</b>
        </Typography>
        <CellContainer>
          {new Array(capacity).fill(0).map((_, i) => (
            <LineContainer key={i}>
              <Typography>
                {i}:{" "}
                {memory[i]?.value ?? (
                  <span style={{ fontStyle: "italic" }}>empty</span>
                )}
              </Typography>
            </LineContainer>
          ))}
        </CellContainer>
      </CardContent>
    </Container>
  );
};

const Container = styled(Card)`
  ${({ theme }) => css`
    margin: ${theme.spacing(1)}px;
  `}
`;

const CellContainer = styled.div`
  height: 100px;
  overflow-y: auto;

  ${({ theme }) => css`
    margin-top: ${theme.spacing(2)}px;
  `}
`;

const LineContainer = styled.div``;

export default CellModule;
