import React, { useRef, useState } from "react";
import { Card, Switch, Typography } from "@material-ui/core";
import styled, { css } from "styled-components";

import { useRegisteredModule } from "Utils/hooks";
import Value from "Simulator/Value";

type Props = {
  index: number;
};

const SwitchModule = ({ index }: Props) => {
  const [state, setState] = useState<boolean>(false);
  const stateRef = useRef<typeof state>(state);
  stateRef.current = state;

  useRegisteredModule({
    type: "switch",
    index,
    interface: {
      getState: () => new Value(stateRef.current),
    },
  });

  return (
    <Container variant="outlined">
      <Typography>
        <b>switch{index}</b>
      </Typography>
      <Switch value={state} onChange={(e) => setState(e.target.checked)} />
    </Container>
  );
};

const Container = styled(Card)`
  display: flex;
  justify-content: space-between;
  align-items: center;

  ${({ theme }) => css`
    margin: ${theme.spacing(1)}px;
    padding: ${theme.spacing(2)}px;
  `}
`;

export default SwitchModule;
