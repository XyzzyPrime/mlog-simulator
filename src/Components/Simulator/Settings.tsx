import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  ButtonGroup,
  Card,
  CardActions,
  CardContent,
  TextField,
} from "@material-ui/core";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import StopIcon from "@material-ui/icons/Stop";
import { observer } from "mobx-react";
import styled, { css } from "styled-components";

import { useSimulatorStore } from "Stores/SimulatorStore";

const Settings = () => {
  const store = useSimulatorStore();
  const [speed, setSpeed] = useState(String(store.instructionPerSecond));

  useEffect(() => {
    const value = Number(speed.trim());
    if (!isNaN(value)) {
      // Speed must be at least 1
      store.instructionPerSecond = Math.max(1, value);
    }
  }, [speed, store]);

  return (
    <Container variant="outlined">
      <CardContent>
        <StyledTextField
          value={speed}
          onChange={(e) => setSpeed(e.target.value)}
          variant="outlined"
          label="Instructions per second"
          type="number"
          onBlur={() => {
            setSpeed(String(store.instructionPerSecond));
          }}
        />
        {Object.keys(store.moduleCounts)
          .map((key) => key as keyof typeof store["moduleCounts"])
          .map((key) => (
            <Box pt={1} key={key}>
              <ButtonGroup>
                <Button
                  color="primary"
                  onClick={() => store.moduleCounts[key]++}
                >
                  Add {key} module
                </Button>
                <Button
                  color="secondary"
                  disabled={store.moduleCounts[key] <= 0}
                  onClick={() => store.moduleCounts[key]--}
                >
                  Remove
                </Button>
              </ButtonGroup>
            </Box>
          ))}
      </CardContent>
      <CardActions>
        <Space />
        {store.state === "running" ? (
          <Button
            color="secondary"
            variant="contained"
            startIcon={<StopIcon />}
            onClick={() => store.stopCode()}
          >
            Stop
          </Button>
        ) : (
          <Button
            color="primary"
            variant="contained"
            startIcon={<PlayArrowIcon />}
            onClick={() => store.runCode()}
          >
            Run
          </Button>
        )}
      </CardActions>
    </Container>
  );
};

const Container = styled(Card)`
  flex-grow: 1;

  ${({ theme }) => css`
    margin: ${theme.spacing(1)}px;
  `}
`;

const StyledTextField = styled(TextField)`
  ${({ theme }) => css`
    // margin: ${theme.spacing(1)}px;
  `}
`;

const Space = styled.div`
  flex-grow: 1;
`;

export default observer(Settings);
