import { observer } from "mobx-react";
import styled from "styled-components";

import { useSimulatorStore } from "Stores/SimulatorStore";

import DisplayModule from "./Modules/DisplayModule";
import CellModule from "./Modules/CellModule";
import MessageModule from "./Modules/MessageModule";
import Settings from "./Settings";
import SwitchModule from "./Modules/SwitchModule";

const Container = styled.div`
  flex-grow: 1;
  height: 100%;
  width: 100%;
`;

const ModulesContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const Simulator = observer(() => {
  const store = useSimulatorStore();

  return (
    <Container>
      <Settings />
      <ModulesContainer>
        {new Array(store.moduleCounts.switch).fill(0).map((_, i) => (
          <SwitchModule key={i} index={i + 1} />
        ))}
        {new Array(store.moduleCounts.message).fill(0).map((_, i) => (
          <MessageModule key={i} index={i + 1} />
        ))}
        {new Array(store.moduleCounts.display).fill(0).map((_, i) => (
          <DisplayModule key={i} index={i + 1} />
        ))}
        {new Array(store.moduleCounts.cell).fill(0).map((_, i) => (
          <CellModule key={i} index={i + 1} />
        ))}
      </ModulesContainer>
    </Container>
  );
});

export default Simulator;
