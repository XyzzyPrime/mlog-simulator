import React from "react";
import { AppBar, Button, Toolbar as MuiToolbar } from "@material-ui/core";
import BugReportIcon from "@material-ui/icons/BugReport";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import styled, { css } from "styled-components";

const Toolbar = () => {
  return (
    <Container>
      <AppBar position="static">
        <MuiToolbar>
          <Button
            color="inherit"
            startIcon={<BugReportIcon />}
            href="https://gitlab.com/JanKaifer/mlog-simulator/-/issues"
          >
            Report bug
          </Button>
          <Button
            color="inherit"
            startIcon={<InfoOutlinedIcon />}
            href="https://gitlab.com/JanKaifer/mlog-simulator/"
          >
            About simulator
          </Button>

          <Space />
        </MuiToolbar>
      </AppBar>
    </Container>
  );
};

const Container = styled.div`
  ${({ theme }) => css`
    margin-bottom: ${theme.spacing(1)}px;

    & .MuiButtonBase-root {
      margin: 0 ${theme.spacing(2)}px;
    }
  `}
`;

const Space = styled.div`
  flex-grow: 1;
`;

export default Toolbar;
