import React from "react";
import styled from "styled-components";

import Editor from "./Editor";
import Simulator from "./Simulator/Simulator";
import Toolbar from "./Toolbar";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;

const ContentContainer = styled.div`
  display: flex;
  flex-grow: 1;
`;

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-grow: 1;
`;

const EditorWrapper = styled(Wrapper)`
  width: 50%;
  padding-bottom: 25px;
`;

const SimulatorWrapper = styled(Wrapper)`
  width: 50%;
`;

const Layout = () => {
  return (
    <Container>
      <Toolbar />
      <ContentContainer>
        <EditorWrapper>
          <Editor />
        </EditorWrapper>
        <SimulatorWrapper>
          <Simulator />
        </SimulatorWrapper>
      </ContentContainer>
    </Container>
  );
};

export default Layout;
