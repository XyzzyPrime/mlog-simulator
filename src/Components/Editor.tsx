import React, { useEffect, useRef, useState } from "react";
import MonacoEditor from "@monaco-editor/react";
import styled from "styled-components";

import { TError } from "Simulator";
import { useSimulatorStore } from "Stores/SimulatorStore";

const EditorContainer = styled.div`
  width: 100%;
  height: 100%;
`;

const Editor = () => {
  const monacoRef = useRef<any>();
  const editorRef = useRef<any>();
  const [error, setError] = useState<TError | null>(null);

  (window as any).setError = setError;

  const simulatorStore = useSimulatorStore();

  useEffect(() => {
    simulatorStore.registerEditor(
      () => editorRef.current?.getValue() ?? "",
      setError
    );
  }, [simulatorStore]);

  useEffect(() => {
    const monaco = monacoRef.current;
    const editor = editorRef.current;
    if (monaco !== undefined && editor !== undefined) {
      monaco.editor.setModelMarkers(
        editor.getModel(),
        "errors",
        error === null
          ? []
          : [
              {
                startLineNumber: error.lineNumber,
                startColumn: 0,
                endLineNumber: error.lineNumber,
                endColumn: 1000,
                message: error.message,
                severity: monaco.MarkerSeverity.Error,
              },
            ]
      );
    }
  }, [error]);

  return (
    <EditorContainer>
      <MonacoEditor
        onMount={(editor, monaco) => {
          editorRef.current = editor;
          monacoRef.current = monaco;
        }}
        defaultLanguage="plaintext"
        defaultValue={`
# This sample program will print prime numbers (this algorithm is really slow, but good enough for demo)
# 0
set tested_number 2
set primes_found 0

# This part checks if tested_number is prime
# 2
set index 0
jump 9 greaterThanEq index primes_found
read prime_at_index cell1 index
op mod remainder tested_number prime_at_index
# check is prime_at_index divides testet_number
jump 14 equal remainder 0 
op add index index 1
jump 3 always 

# It is prime
# 9
write tested_number cell1 primes_found
op add primes_found primes_found 1
print "found prime "
print tested_number
printflush message1

# Lets try different numbers
# 14
op add tested_number tested_number 1

# 15
jump 2 always
`}
      />
    </EditorContainer>
  );
};

export default Editor;
