import { createContext } from "react";
import { makeObservable, observable } from "mobx";

import Simulator, { TError, TModule } from "Simulator";
import { useSafeContext } from "Utils/hooks";
import { uuid } from "Utils/misc";

class SimulatorStore {
  state: "running" | "stopped" = "stopped";
  simulator = new Simulator();
  registeredModules: Record<string, TModule> = {};
  getText = () => "";
  setError = (_: TError | null) => {};
  protected stopSimulator: (() => void) | null = null;
  instructionPerSecond = 100;

  moduleCounts: Record<TModule["type"], number> = {
    message: 1,
    display: 0,
    cell: 1,
    switch: 0,
  };

  constructor() {
    makeObservable(this, {
      registeredModules: observable,
      instructionPerSecond: observable,
      state: observable,
      moduleCounts: observable,
    });
  }

  registerEditor = (
    getText: () => string,
    setError: (error: TError | null) => void
  ) => {
    this.getText = getText;
    this.setError = setError;
  };

  registerModule = (module: TModule) => {
    const moduleId = uuid();
    this.registeredModules[moduleId] = module;
    return moduleId;
  };

  unregisterModule = (moduleId: string) => {
    delete this.registeredModules[moduleId];
  };

  runCode = async () => {
    if (this.state !== "stopped") return;

    this.setError(null);
    this.state = "running";
    const machine = this.simulator.runCode({
      code: this.getText(),
      modules: Object.values(this.registeredModules),
      setError: this.setError,
      instructionsPerSecond: this.instructionPerSecond,
      onStop: () => (this.state = "stopped"),
    });

    if (machine !== null) {
      this.stopSimulator = () => machine.stop();
    }
  };

  stopCode = () => {
    (this.stopSimulator ?? (() => {}))();
  };
}

export const simulatorStoreContext = createContext<SimulatorStore | null>(null);
export const useSimulatorStore = () => useSafeContext(simulatorStoreContext);

export type { TModule };

export default SimulatorStore;
