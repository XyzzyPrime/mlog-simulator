export const isNullish = <T>(value: T): value is NonNullable<T> =>
  value === undefined || value === null;

export const assert = (value: boolean): asserts value is true => {
  if (!value) {
    throw new Error("Assertion error");
  }
};

export function assertNotNullish<T>(value: T): asserts value is NonNullable<T> {
  return assert(!isNullish(value));
}
