import React, { useContext, useEffect } from "react";

import { TModule, useSimulatorStore } from "Stores/SimulatorStore";

import { assertNotNullish } from "./typegurads";

export const useSafeContext = <T>(context: React.Context<T>) => {
  const value = useContext(context);

  assertNotNullish(value);

  return value;
};

export const useRegisteredModule = (module: TModule, deps = []) => {
  const simulatorStore = useSimulatorStore();

  useEffect(() => {
    const moduleId = simulatorStore.registerModule(module);

    return () => simulatorStore.unregisterModule(moduleId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, deps);
};
