# Mindustry logic simulator

This simulator simulates Mindustry logic instructions with Typescript in your browser.
You can try it out on [mlogsim.kaifer.cz](https://mlogsim.kaifer.cz).

This simulator is not an exact reimplementation of the Mindustry logic engine.
It will for example abort execution due to type errors (arithmetics between different types like `1 + null` or `"string" + 42`). 

## Motivation
Mindustry logic is awesome game mechanics.
Debugging longer code in-game is an unpleasant experience (at least for me), so I decided to create a simple runtime on the web.
This simulator is ideal for testing code that does any computations or graphical/textual output.
It can be also used when testing compilers like [Mindcode](https://www.reddit.com/r/Mindustry/comments/m60qli/mindcode_a_higher_level_language_that_compiles/) or [mpro compiler (not yet public)](https://github.com/Antyos/mpro-compiler).

## TODO list
I want to implement these features (no promises):

- [x] - usable editor
- [x] - basic commands: `op`, `set`, `jump`
- [x] - text output: `print`, `printflush`
- [x] - graphical output: `draw`, `drawflush`
- [x] - support for memory cells: `read`, `write`
- [x] - support for switches

## NOT on my TODO list
I won't implement this stuff (You can, PRs are welcome):

- emulation of units, turrets, or other non-logic entities

## Documentation of supported subset of mindustry language

### Language primitives
- every line represents one instructions, lines are ordered from 0 and each has unique number
- every instruction consists of multiple words, there exist 3 types of words:
    - 'keyword' variable-like name that does not tries to evaluate to value (for example `add` in `op` call)
    - variable - name that is made of letters, '@' or numbers (can't start with number)
    - constant - `"some string"` or number like `22234`
- every instruction has fixed order of parameters and different instructions expect different parameter type on different position

### Special variables
- `@counter` - number of next executed instruction, can be modified by program
- `false` and `true` - boolean constant

### instructions

#### `op [operationName] [outputVariable] [arg1] [arg2]`
- This operation does basic math operations
- `operationName` is one of `add`, `sub`, `mul`, ... many others, complete list in [this simulator](https://gitlab.com/JanKaifer/mlog-simulator/-/blob/master/src/Simulator/Operations.ts#L35) and [mindustry source](https://github.com/Anuken/Mindustry/blob/master/core/src/mindustry/logic/LogicOp.java)
- `outputVariable` has to be a variable
- `arg1` and `arg2` can be constants or variables, and many operations take only `arg1`


#### `set [outputVariable] [arg1]`
- assigns value of `arg1` to variable `outputVariable`


#### `jump [instructionNumber] [operationName] [arg1] [arg2]`
- very similar to `op`
- if `operationName` on `arg1` and `arg2` return `true`, than it will jump to instruction number `instructionNumber` (assigns `instructionNumber` to `@counter`)
- `operationName` must return `boolean`


#### `print [arg1]`
- adds value of `arg1` to print dequeue (doesn't print anything yet)


#### `printflush [arg1]`
- flushes all text in print queue to message link specified in `arg1`
- `arg1` has to be valid message link, ie `message1`

#### `draw [operationName] ...`
- This operation does drawing to screens
- All colors and alphas should be in range `0-255`
- The following operations are supported:
    - `draw clear [red] [green] [blue]` - fills whole display with this color
    - `draw color [red] [green] [blue] [alpha]` - sets used color
    - `draw stroke [width]` - sets border/stroke width
    - `draw line [x1] [y1] [x2] [y2]` - draws line between these points
    - `draw rect [x] [y] [width] [height]` - draw this rect (fill)
    - `draw linerect [x] [y] [width] [height]` - draw this rect (only border)
- The following operations exist in mindustry but are not supported by this emulator:
    - `draw poly [x] [y] [sides] [radius] [rotation]` - draw this polygon (fill)
    - `draw linepoly [x] [y] [sides] [radius] [rotation]` - draw this polygon (only border)
    - `draw triangle [x] [y] [x2] [y2] [x3] [y3]` - draws this triangle (fill)
    - `draw image [x] [y] [image] [size] [rotation]` - draws this in-game entity specified in `[image]` like `@router` or `@copper`

#### `drawflush [arg1]`
- flushes all text in draw queue to display link specified in `arg1`
- `arg1` has to be valid message link, ie `display1`

#### `write [val] [link] [index]`
- saves `[val]` to memory cell with link `[link]` on position `[index]`

#### `read [var] [link] [index]`
- saves value from memory cell with link `[link]` on position `[index]` to variable `[var]`

#### `sensor [result] [link] [prop]`
- read property `[prop]` of entity with link `[link]` and saves it to `[result]`
- currently supported only reading of `@enabled` of switch
- example: `sensor boolVariable switch1 @enabled`

## Contributing/Development

This is simple CRA React app, to develop do this:

- Install `yarn`
- Run `yarn` in root of this project
- Run `yarn start` in root of this project (opens browser with running project)
- Code and se your changes live in the browser
